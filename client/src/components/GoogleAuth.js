import React, { Component } from "react";
import { connect } from "react-redux";
import { signIn, signOut } from "../actions";

class GoogleAuth extends Component {
  /* The reason to make it null, is that we don't know
     whether the user is logged in or out when our app first loads up */
  //state = { isSignedIn: null };

  /* This is the initializing the process of Google Auth API */
  componentDidMount() {
    // it takes sometime load the library from google so
    // we need a callback when it is done loading
    window.gapi.load("client:auth2", () => {
      /* Initializing client library
       "window.gapi.client.init({})" => This function returns a "Promise"
       */
      window.gapi.client
        .init({
          clientId:
            "308749566590-t6978hm62idaa8cao6bp1rsvs6k0jspe.apps.googleusercontent.com",
          scope: "email"
        })
        .then(() => {
          // setting the instance from async result to class instance "this"
          this.auth = window.gapi.auth2.getAuthInstance();

          this.onAuthChanged(this.auth.isSignedIn.get());

          /* This is event listner and listens when the user oAuth status changes */
          this.auth.isSignedIn.listen(this.onAuthChanged);
        });
    });
  }

  onAuthChanged = isSignedIn => {
    // Calling Action creators base on the signin or signout status
    if (isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };
  /* This is a callback, so don't call it with () down in the onClick="" */
  onSignIn = () => {
    this.auth.signIn();
  };
  /* This is a callback, so don't call it with () down in the onClick="" */
  onSignOut = () => {
    this.auth.signOut();
  };
  renderAuthButton = () => {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <button onClick={this.onSignOut} className="ui red google button">
          <i className="google icon" />
          Sign Out
        </button>
      );
    } else {
      return (
        <button onClick={this.onSignIn} className="ui green google button">
          <i className="google icon" />
          Sign In with Google
        </button>
      );
    }
  };

  render() {
    //console.log("I am inside GoogleAuth render()");
    return <div>{this.renderAuthButton()}</div>;
  }
}
const mapStateToProps = (state) => {
  return {
    isSignedIn: state.auth.isSignedIn
  }
}

export default connect(
  mapStateToProps,
  { signIn, signOut }
)(GoogleAuth);
