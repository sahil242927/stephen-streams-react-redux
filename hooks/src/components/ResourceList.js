import React from "react";
import useResources from "./useResources";

const ResourceList = ({ resource }) => {
  const resources = useResources(resource);
  return (
    <div>
      <ul className="ui list">
        {resources.map(record => (
          <li className="item" key={record.id}>
            {record.title}
          </li>
        ))}
      </ul>
    </div>
  );
};
export default ResourceList;
